/**
 * @module
 */

var mapResult = function(data) {
  if (data.error) {
    return "(" + data.error.line + ':' + data.error.character + ') ' + data.error.reason;
  }
};

exports.run = function(file) {
  if (file.jshint.success) {
    return false;
  }

  var errors = file.jshint.results.map(mapResult).join("\n");
  return file.relative + " (" + file.jshint.results.length + " errors)\n" + errors;
};

module.exports = exports;
