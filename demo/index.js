(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
/**
 * Controller view: database
 * @module
 */

/**
 *
 * @constructor
 */
var Mdl = function(data, ctr) {
  Object.keys(data).forEach(this.buildProp.bind(this, data));

  this.ctr = ctr;
};

/**
 * Build a prop for a model
 */
Mdl.prototype.buildProp = function(data, propKey) {
  this[propKey] = data[propKey];
};

Mdl.prototype.loadGame = function() {
  // position of ducks generated on the server side
  //   or in this controller
  var demoDataFromDb = {
    id: 1,
    time_left: 100,
    field_width: 100, // mm
    field_height: 100, // mm
    ducks: [{
      id: 200,
      pos_x: 40, // mm
      pos_y: 59, // mm
      color: '#555',
      width: 12,
      height: 12, // mm
      is_alive: true,
      id_of_game: 1
    }, {
      id: 201,
      pos_x: 50,
      pos_y: 25,
      color: '#999',
      width: 15, //mm
      height: 15, // mm
      is_alive: true,
      id_of_game: 1
    }],
    hunters: []
  };

  this.ctr.initModel(demoDataFromDb);
  for (var i = 0; i < demoDataFromDb.ducks.length; i++) {
    this.ctr.model.addDuck(demoDataFromDb.ducks[i]);
  }
};

/**
 * Create an instance
 */
exports.init = function() {
  var obj = Object.create(Mdl.prototype);
  Mdl.apply(obj, arguments);
  return obj;
};

module.exports = exports;

},{}],2:[function(require,module,exports){
/**
 * Controller view: random flight for all ducks
 * @module
 */

var jqr = require('./helpers/jqr');
var rnd = require('./helpers/rnd');

/**
 *
 * @constructor
 */
var Mdl = function(data, ctr) {
  this.ctr = ctr;
};

/**
 * Generate duck position
 */
Mdl.prototype.gnrtDuckPosition = function(duckItem) {
  var duckData = jqr.extend({}, duckItem);
  duckData.pos_x = rnd.between(0, this.ctr.model.field_width);
  duckData.pos_y = rnd.between(0, this.ctr.model.field_height);
  duckItem.updateModel(duckData);
};

/**
 * Generate random positions for all ducks
 */
Mdl.prototype.gnrtDucksPosition = function() {
  this.ctr.model.ducks.forEach(this.gnrtDuckPosition.bind(this));
};

/**
 * Start a process of flight
 * @param {Number} updateInterval - Update interaval, in seconds
 */
Mdl.prototype.startFlight = function(updateInterval) {
  window.setInterval(this.gnrtDucksPosition.bind(this), updateInterval * 1000);
};

Mdl.prototype.gnrtGameTimeLeft = function(tint) {
  var modelData = jqr.extend({}, this.ctr.model);
  if (modelData.time_left > 0) {
    modelData.time_left = modelData.time_left - 1;

    this.ctr.model.updateModel(modelData);
  } else {
    window.clearInterval(tint);
  }
};

/**
 * Start a timer
 */
Mdl.prototype.startTimer = function() {
  var tint = window.setInterval(this.gnrtGameTimeLeft.bind(this, tint), 1000);
};

/**
 * Create an instance
 */
exports.init = function() {
  var obj = Object.create(Mdl.prototype);
  Mdl.apply(obj, arguments);
  return obj;
};

module.exports = exports;

},{"./helpers/jqr":5,"./helpers/rnd":7}],3:[function(require,module,exports){
/**
 * Controller view: UI in browser
 *    Every controller view uses functions from a controller
 *    For example: pause/start
 *    Controller view only for game - not for every duck
 *    A duck can contain other controller-view (specific for every duck)
 * @module
 */

var jqr = require('./helpers/jqr');

/**
 *
 * @constructor
 */
var Mdl = function(data, ctr) {
  this.ctr = ctr;

  this.block = document.createElement('div');

  /**
   * Aspect ratio of view
   *    must be equal to model-view-ui
   *    can be moved from initiator (main code)
   *    ~4px in 1cm
   */
  this.aspect_ratio = 4;
  //  this.inp = document.createElement('input');
  //  this.btn = document.createElement('button');

  //jqr(this.btn).on('click', this.handleInput.bind(this));
  // jqr(this.inp).on('change', this.handleInput.bind(this));
  this.setConstPart();
};

Mdl.prototype.setConstPart = function() {
  jqr(this.block)
    .css({
      width: this.ctr.model.field_width * this.aspect_ratio,
      height: this.ctr.model.field_height * this.aspect_ratio
    })
    .on('click', this.handleBlockClick.bind(this));
};

/**
 * Disable a duck if in sight
 */
Mdl.prototype.checkDuck = function(coordX, coordY, mdlDuck) {
  if (mdlDuck.isDuckInSight(coordX, coordY)) {
    // update a model duck
    var duckData = jqr.extend({}, mdlDuck);
    duckData.is_alive = false;
    mdlDuck.updateModel(duckData);
//    console.log('duck in sight');
  }
};

Mdl.prototype.handleBlockClick = function(e) {
  console.log('event', e);
  // convert coordinates to model's
  // offset coordinate system from left-right (X) and top-bottom(Y) - change it
  var coordX = e.offsetY / this.aspect_ratio,
    coordY = e.offsetX / this.aspect_ratio;
  console.log('coords', coordX, coordY);
  //  this.ctr.model.makeShoot(coordX, coordY);
  this.ctr.model.ducks.forEach(this.checkDuck.bind(this, coordX, coordY));

  // a. get all ducks - calculate matches - disable ducks (might be few ducks per point)
  // b. send params to the model - disable ducks there
};

/**
 * Create an instance
 */
exports.init = function() {
  var obj = Object.create(Mdl.prototype);
  Mdl.apply(obj, arguments);
  return obj;
};

module.exports = exports;

},{"./helpers/jqr":5}],4:[function(require,module,exports){
/**
 * Controller example
 * @module
 */

var hprGame = require('./models/game');

/**
 *
 * @constructor
 */
var Mdl = function(data) {
  Object.keys(data).forEach(this.buildProp.bind(this, data));

  /**
   * Model creates after receiving a data
   */
  this.model = null;
};

/**
 * Build a prop for a model
 */
Mdl.prototype.buildProp = function(data, propKey) {
  this[propKey] = data[propKey];
};

/**
 * Create an instance of a model
 */
Mdl.prototype.initModel = function(data) {
  this.model = hprGame.init(data);
  console.log(this.model);
};

/**
 * Create an instance
 */
exports.init = function() {
  var obj = Object.create(Mdl.prototype);
  Mdl.apply(obj, arguments);
  return obj;
};

module.exports = exports;

},{"./models/game":10}],5:[function(require,module,exports){
/**
 * Export jquery
 *   can be changed to other module
 */
module.exports = $;

},{}],6:[function(require,module,exports){
// A generic observable subject class that is useful in model creation.
//
exports.makeObservableSubject = function() {
  var observers = [];

  var addObserver = function(o) {
    if (typeof o !== 'function') {
      throw new Error('observer must be a function');
    }
    for (var i = 0, ilen = observers.length; i < ilen; i++) {
      var observer = observers[i];
      if (observer === o) {
        throw new Error('observer already in the list');
      }
    }
    observers.push(o);
  };

  var removeObserver = function(o) {
    for (var i = 0, ilen = observers.length; i < ilen; i++) {
      var observer = observers[i];
      if (observer === o) {
        observers.splice(i, 1);
        return;
      }
    }
    throw new Error('could not find observer in list of observers');
  };

  var notifyObservers = function(data) {
    // Make a copy of observer list in case the list
    // is mutated during the notifications.
    var observersSnapshot = observers.slice(0);
    for (var i = 0, ilen = observersSnapshot.length; i < ilen; i++) {
      observersSnapshot[i](data);
    }
  };

  return {
    addObserver: addObserver,
    removeObserver: removeObserver,
    notifyObservers: notifyObservers
  };
};

module.exports = exports;

},{}],7:[function(require,module,exports){
exports.between = function(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
};

module.exports = exports;

},{}],8:[function(require,module,exports){
var hprCtr = require('./ctr');
var hprCtrDb = require('./ctr-db');
var hprCtrRand = require('./ctr-rand');
var hprGameViewUi = require('./views/game-view-ui');
var hprCtrUi = require('./ctr-ui');

var jqr = require('./helpers/jqr');

/**
 * Controller can exists without views
 */
var ctr = hprCtr.init({});

/**
 * Controller view: load initial data from db
 *    Only read from db -> send to model
 */
var ctrDb = hprCtrDb.init({}, ctr);
ctrDb.loadGame();

var gameViewUi1 = hprGameViewUi.init({
  background: 'lightgreen'
}, ctr.model);

gameViewUi1.addDuckViews();
jqr('body').css({
  padding: '10px'
});
jqr('body').append(gameViewUi1.block);

var ctrRand = hprCtrRand.init({}, ctr);
ctrRand.startTimer();
ctrRand.startFlight(3);

/**
 * Controller views can not exists without controllers
 * @todo: possible creation in a controller
 */
var ctrUi = hprCtrUi.init({}, ctr);
var absWrap = document.createElement('div');
jqr(absWrap).css({
  position: 'absolute'
});
jqr('body').append(absWrap);
absWrap.appendChild(ctrUi.block);



},{"./ctr":4,"./ctr-db":1,"./ctr-rand":2,"./ctr-ui":3,"./helpers/jqr":5,"./views/game-view-ui":13}],9:[function(require,module,exports){
/**
 * Duck
 * @module
 */

var obs = require('../helpers/obs');

var checkData = function(data, game) {
  var errors = [];
  if (data.pos_x < 0 || data.pos_x > game.field_width) {
    errors.push(new Error('required: pos_x in perimeter: 0-' + game.field_width));
  }

  if (data.pos_y < 0 || data.pos_y > game.field_height) {
    errors.push(new Error('required: pos_y in perimeter: 0-' + game.field_height));
  }

  return errors;
};

/**
 * Duck
 * @constructor
 */
var Mdl = function(data, game) {
  this.getGame = function() {
    return game;
  };

  this.applyProps(data);

  this.obsSubject = obs.makeObservableSubject();
};

/**
 * Apply main props for a model
 *     do not use a game parameter
 */
Mdl.prototype.applyProps = function(data) {
  /**
   * Coords of a duck, mm
   */
  this.pos_x = data.pos_x;
  this.pos_y = data.pos_y;

  /**
   * Whether the duck alive, show or hide on the screen
   * @type {boolean}
   */
  this.is_alive = data.is_alive;

  /**
   * Id of a game, from a storage
   * @type {Number}
   */
  this.id_of_game = data.id_of_game;

  /**
   * Color of a duck, #rgb format
   *    unchangeable after initialization
   * @type {String}
   */
  this.color = data.color;

  /**
   * Width of a duck, in mm
   */
  this.width = data.width;

  /**
   * Height of a duck, in mm
   */
  this.height = data.height;
};

/**
 * Update a whole model
 *    instead separated props
 */
Mdl.prototype.updateModel = function(data) {
  var errors = checkData(data, this.getGame());
  if (errors.length > 0) {
    throw new Error('errors: ' + errors.join(';'));
  }

  this.applyProps(data);
  this.obsSubject.notifyObservers();
};

/**
 * Whether coord in sight
 * @return {Boolean}
 */
var isInBorder = function(sight, min, max) {
  if ((sight > min) && (sight < max)) {
    return true;
  }
  return false;
};

/**
 * Whether a duck in sight
 */
Mdl.prototype.isDuckInSight = function(sightX, sightY) {
  //  console.log('duck-posx', this.pos_y, sightY);
  if (isInBorder(sightX, this.pos_x, this.pos_x + this.width)) {
    //console.log('x in border');
    if (isInBorder(sightY, this.pos_y, this.pos_y + this.height)) {
      //      console.log('in bordder');
      return true;
    }
  }

  return false;
};

/**
 * Create an instance
 */
exports.init = function(data, game) {
  // some of data can depends of a parent
  var errors = checkData(data, game);
  if (errors.length > 0) {
    throw new Error('errors: ' + errors.join(';'));
  }

  var obj = Object.create(Mdl.prototype);
  Mdl.apply(obj, [data, game]);
  obj.obsSubject.notifyObservers();
  return obj;
};

module.exports = exports;

},{"../helpers/obs":6}],10:[function(require,module,exports){
/**
 * Model example
 * @module
 */

var hprHunter = require('./hunter.js');
var hprDuck = require('./duck.js');
var obs = require('../helpers/obs');

/**
 * Input data checked, using metadata for this model
 */
var checkData = function(data) {
  var errors = [];
  if (!data.id) {
    errors.push(new Error('required: id'));
  }
  if (data.time_left < 0) {
    errors.push(new Error('required: time_left: positive'));
  }
  return errors;
};

/**
 *
 * @constructor
 */
var Mdl = function(data) {
  this.applyProps(data);

  /** 
   * Add ducks
   *    It is not a part of a model: add it separatelly
   *    through method: addDuck, addHunter
   *    if an error will be catched: do not add only one item, but a model stay be created
   *    Not properties of a model - like calculated properties
   */
  this.ducks = [];
  this.hunters = [];

  this.obsSubject = obs.makeObservableSubject();
};

/**
 * Make an observable model
 */
Mdl.prototype.applyProps = function(data) {
  /**
   * Id of a game
   * @type {Number}
   */
  this.id = data.id;

  /**
   * Time for hunting, in seconds
   * @type {Number}
   */
  this.time_left = data.time_left;

  /**
   * Width of a game
   */
  this.field_width = data.field_width;

  /**
   * Height of a game, in mm
   * @type {Number}
   */
  this.field_height = data.field_height;
};

/**
 * External method to update all model at one time
 */
Mdl.prototype.updateModel = function(data) {
  var errors = checkData(data);
  if (errors.length > 0) {
    return new Error('errors: ' + errors.join(';'));
  }
  this.applyProps(data);
  this.obsSubject.notifyObservers();
  return this;
};

/**
 * Init a duck
 */
Mdl.prototype.initDuck = function(data) {
  return hprDuck.init(data, this);
};

Mdl.prototype.addDuck = function(data) {
  var duckNew = this.initDuck(data);
  // catch error
  this.ducks.push(duckNew);
};

Mdl.prototype.initHunter = function(data) {
  return hprHunter.init(data, this);
};

/**
 * Create an instance
 *    Check data before creation and check every time during update
 */
exports.init = function(data) {
  var errors = checkData(data);
  if (errors.length > 0) {
    throw new Error('errors: ' + errors.join(';'));
  }

  var obj = Object.create(Mdl.prototype);
  Mdl.apply(obj, arguments);
  //  return obj;

  // every time, when model is changed - notify views
  obj.obsSubject.notifyObservers();
  return obj;
};

module.exports = exports;

},{"../helpers/obs":6,"./duck.js":9,"./hunter.js":11}],11:[function(require,module,exports){
/**
 * Hunter model
 * @module
 */

/**
 * Hunter
 * @constructor
 */
var Mdl = function(data, game) {
  this.getGame = function() {
    return game;
  };
  this.id = data.id;
  this.name = data.name;
  this.score = data.score;
  this.cursor_color = data.cursor_color;
  this.id_of_game = data.id_of_game;
};

/**
 * Create an instance
 */
exports.init = function() {
  var obj = Object.create(Mdl.prototype);
  Mdl.apply(obj, arguments);
  return obj;
};

module.exports = exports;

},{}],12:[function(require,module,exports){
/**
 * Duck view UI
 * @module
 */

var jqr = require('../helpers/jqr');

/**
 * Duck
 * @constructor
 */
var Mdl = function(data, modelDuck, gameView) {
  this.getDuckModel = function() {
    return modelDuck;
  };

  this.getGameView = function() {
    return gameView;
  };

  this.block = document.createElement('div');
  this.duckElem = document.createElement('div');
  this.block.appendChild(this.duckElem);

  this.fillConstPart();
};

/**
 * Fill constant part of a view
 * @private
 */
Mdl.prototype.fillConstPart = function() {
  jqr(this.block).css({
    position: 'absolute'
  });

  var aspectRatio = this.getGameView().aspect_ratio;

  // unchangeable properties from mdlDuck
  jqr(this.duckElem).css({
    background: this.getDuckModel().color,
    width: this.getDuckModel().width * aspectRatio,
    height: this.getDuckModel().height * aspectRatio
  });


  this.getDuckModel().obsSubject.addObserver(this.fillDynamicPart.bind(this));
};

/** 
 * Handle model: fill dynamic props
 * @private
 */
Mdl.prototype.fillDynamicPart = function() {
  if (this.getDuckModel().is_alive === true) {
    var aspectRatio = this.getGameView().aspect_ratio;

    // changeable properties of model
    jqr(this.block).animate({
      top: this.getDuckModel().pos_x * aspectRatio,
      left: this.getDuckModel().pos_y * aspectRatio
    }, 400);
  } else {
    // todo: remove a duck or stop observing of a model
    jqr(this.block).hide();
  }
};

/**
 * Create an instance
 */
exports.init = function() {
  var obj = Object.create(Mdl.prototype);
  Mdl.apply(obj, arguments);
  return obj;
};

module.exports = exports;

},{"../helpers/jqr":5}],13:[function(require,module,exports){
/**
 *
 * @module
 */

var jqr = require('../helpers/jqr');
var hprDuckView = require('./duck-view-ui');

/**
 * View depends of a model
 * @constructor
 */
var Mdl = function(data, modelGame) {
  this.getModelGame = function() {
    return modelGame;
  };

  /**
   * Background of a view, #rgb
   *    specific property for views
   * @type {String}
   */
  this.background = data.background;

  /** 
   * Increase coordinates of model: for a game-field and ducks
   *    ~ 4px in 1cm
   */
  this.aspect_ratio = 4; 

  this.block = document.createElement('div');
  this.wrap = document.createElement('div');
  this.block.appendChild(this.wrap);
  this.timer = document.createElement('div');
  this.block.appendChild(this.timer);

  /** 
   * List of duck views
   * @type {Array}
   */
  this.duck_views = [];

  this.fillConstPart();
  return this;
};

/**
 * Fill unchangeable properties
 * @private
 */
Mdl.prototype.fillConstPart = function() {
  jqr(this.block).css({
    float: 'left'
  });

  jqr(this.wrap).css({
    background: this.background,
    position: 'relative'
  });

  var viewFieldWidth = this.getModelGame().field_width * this.aspect_ratio;
  var viewFieldHeight = this.getModelGame().field_height * this.aspect_ratio;

  jqr(this.wrap).css({
    width: viewFieldWidth,
    height: viewFieldHeight
  });

  /**
   * Add dynamic properties when a model is changed
   */
  this.getModelGame().obsSubject.addObserver(this.fillDynamicPart.bind(this));
};

/**
 * Fill dynamic parts of a view
 * @private
 */
Mdl.prototype.fillDynamicPart = function() {
  this.timer.innerHTML = this.getModelGame().time_left;
};

/**
 * Add a view of a duck
 */
Mdl.prototype.addDuckView = function(modelDuck) {
  var duckView = hprDuckView.init({}, modelDuck, this);
  this.duck_views.push(duckView);
  // draw a duck in a field
  this.wrap.appendChild(duckView.block);

};

/**
 * Generate duck views from models
 */
Mdl.prototype.addDuckViews = function() {
  this.getModelGame().ducks.forEach(this.addDuckView.bind(this));
};

/**
 * Create an instance
 */
exports.init = function() {
  var obj = Object.create(Mdl.prototype);
  Mdl.apply(obj, arguments);
  return obj;
};

module.exports = exports;

},{"../helpers/jqr":5,"./duck-view-ui":12}]},{},[8]);
