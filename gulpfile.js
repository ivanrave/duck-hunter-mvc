/**
 * Gulp tasks
 */

var notify = require('gulp-notify');
var gulpExec = require('gulp-exec');
var gulp = require('gulp');
var jshint = require('gulp-jshint');
var stylish = require('jshint-stylish');
var path = require('path');
var express = require('express');

var helperPath = path.resolve('task-helpers');

var ntfHelper = require(path.join(helperPath, 'jshint-notify'));

var pth = {};
pth.src = path.resolve('src');
pth.dst = path.resolve('demo');

var cbkNotifyOnError = function(err) {
  return err.message;
};

gulp.task('jshint', function() {
  return gulp.src(['./*.js', pth.src + '**/*.js'])
    .pipe(jshint('.jshintrc'))
    .pipe(jshint.reporter(stylish))
    .pipe(notify(ntfHelper.run));
});


var runBwf = function(srcFile, bundleFile) {
  var command = 'browserify <%= file.path %> -o ' + bundleFile;
  //    ' --exclude ' + pth.libs.modernizr +
  //    ' --exclude ' + pth.libs.jquery;

  return gulp.src(srcFile)
    .pipe(gulpExec(command))
    .on('error', notify.onError(cbkNotifyOnError));
};

gulp.task('bwf', ['jshint'], function() {
  return runBwf(path.join(pth.src, 'index.js'), path.join(pth.dst, 'index.js'));
});

gulp.task('build', ['bwf']);

// Rerun the task when a file changes
gulp.task('watch', function() {
  return gulp.watch(path.join(pth.src, '**/*.js'), ['bwf']);
});

function startExpress() {
  var app = express();
  app.use(express.static('./demo/'));
  app.listen(6789);
  console.log('http://localhost:6789');
}

gulp.task('connect', function() {
  startExpress();
});
