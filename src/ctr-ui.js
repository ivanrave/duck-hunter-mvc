/**
 * Controller view: UI in browser
 *    Every controller view uses functions from a controller
 *    For example: pause/start
 *    Controller view only for game - not for every duck
 *    A duck can contain other controller-view (specific for every duck)
 * @module
 */

var jqr = require('./helpers/jqr');

/**
 *
 * @constructor
 */
var Mdl = function(data, ctr) {
  this.ctr = ctr;

  this.block = document.createElement('div');

  /**
   * Aspect ratio of view
   *    must be equal to model-view-ui
   *    can be moved from initiator (main code)
   *    ~4px in 1cm
   */
  this.aspect_ratio = 4;
  //  this.inp = document.createElement('input');
  //  this.btn = document.createElement('button');

  //jqr(this.btn).on('click', this.handleInput.bind(this));
  // jqr(this.inp).on('change', this.handleInput.bind(this));
  this.setConstPart();
};

Mdl.prototype.setConstPart = function() {
  jqr(this.block)
    .css({
      width: this.ctr.model.field_width * this.aspect_ratio,
      height: this.ctr.model.field_height * this.aspect_ratio
    })
    .on('click', this.handleBlockClick.bind(this));
};

/**
 * Disable a duck if in sight
 */
Mdl.prototype.checkDuck = function(coordX, coordY, mdlDuck) {
  if (mdlDuck.isDuckInSight(coordX, coordY)) {
    // update a model duck
    var duckData = jqr.extend({}, mdlDuck);
    duckData.is_alive = false;
    mdlDuck.updateModel(duckData);
//    console.log('duck in sight');
  }
};

Mdl.prototype.handleBlockClick = function(e) {
  console.log('event', e);
  // convert coordinates to model's
  // offset coordinate system from left-right (X) and top-bottom(Y) - change it
  var coordX = e.offsetY / this.aspect_ratio,
    coordY = e.offsetX / this.aspect_ratio;
  console.log('coords', coordX, coordY);
  //  this.ctr.model.makeShoot(coordX, coordY);
  this.ctr.model.ducks.forEach(this.checkDuck.bind(this, coordX, coordY));

  // a. get all ducks - calculate matches - disable ducks (might be few ducks per point)
  // b. send params to the model - disable ducks there
};

/**
 * Create an instance
 */
exports.init = function() {
  var obj = Object.create(Mdl.prototype);
  Mdl.apply(obj, arguments);
  return obj;
};

module.exports = exports;
