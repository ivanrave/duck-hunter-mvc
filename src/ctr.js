/**
 * Controller example
 * @module
 */

var hprGame = require('./models/game');

/**
 *
 * @constructor
 */
var Mdl = function(data) {
  Object.keys(data).forEach(this.buildProp.bind(this, data));

  /**
   * Model creates after receiving a data
   */
  this.model = null;
};

/**
 * Build a prop for a model
 */
Mdl.prototype.buildProp = function(data, propKey) {
  this[propKey] = data[propKey];
};

/**
 * Create an instance of a model
 */
Mdl.prototype.initModel = function(data) {
  this.model = hprGame.init(data);
  console.log(this.model);
};

/**
 * Create an instance
 */
exports.init = function() {
  var obj = Object.create(Mdl.prototype);
  Mdl.apply(obj, arguments);
  return obj;
};

module.exports = exports;
