var hprCtr = require('./ctr');
var hprCtrDb = require('./ctr-db');
var hprCtrRand = require('./ctr-rand');
var hprGameViewUi = require('./views/game-view-ui');
var hprCtrUi = require('./ctr-ui');

var jqr = require('./helpers/jqr');

/**
 * Controller can exists without views
 */
var ctr = hprCtr.init({});

/**
 * Controller view: load initial data from db
 *    Only read from db -> send to model
 */
var ctrDb = hprCtrDb.init({}, ctr);
ctrDb.loadGame();

var gameViewUi1 = hprGameViewUi.init({
  background: 'lightgreen'
}, ctr.model);

gameViewUi1.addDuckViews();
jqr('body').css({
  padding: '10px'
});
jqr('body').append(gameViewUi1.block);

var ctrRand = hprCtrRand.init({}, ctr);
ctrRand.startTimer();
ctrRand.startFlight(3);

/**
 * Controller views can not exists without controllers
 * @todo: possible creation in a controller
 */
var ctrUi = hprCtrUi.init({}, ctr);
var absWrap = document.createElement('div');
jqr(absWrap).css({
  position: 'absolute'
});
jqr('body').append(absWrap);
absWrap.appendChild(ctrUi.block);


