/**
 *
 * @module
 */

var jqr = require('../helpers/jqr');
var hprDuckView = require('./duck-view-ui');

/**
 * View depends of a model
 * @constructor
 */
var Mdl = function(data, modelGame) {
  this.getModelGame = function() {
    return modelGame;
  };

  /**
   * Background of a view, #rgb
   *    specific property for views
   * @type {String}
   */
  this.background = data.background;

  /** 
   * Increase coordinates of model: for a game-field and ducks
   *    ~ 4px in 1cm
   */
  this.aspect_ratio = 4; 

  this.block = document.createElement('div');
  this.wrap = document.createElement('div');
  this.block.appendChild(this.wrap);
  this.timer = document.createElement('div');
  this.block.appendChild(this.timer);

  /** 
   * List of duck views
   * @type {Array}
   */
  this.duck_views = [];

  this.fillConstPart();
  return this;
};

/**
 * Fill unchangeable properties
 * @private
 */
Mdl.prototype.fillConstPart = function() {
  jqr(this.block).css({
    float: 'left'
  });

  jqr(this.wrap).css({
    background: this.background,
    position: 'relative'
  });

  var viewFieldWidth = this.getModelGame().field_width * this.aspect_ratio;
  var viewFieldHeight = this.getModelGame().field_height * this.aspect_ratio;

  jqr(this.wrap).css({
    width: viewFieldWidth,
    height: viewFieldHeight
  });

  /**
   * Add dynamic properties when a model is changed
   */
  this.getModelGame().obsSubject.addObserver(this.fillDynamicPart.bind(this));
};

/**
 * Fill dynamic parts of a view
 * @private
 */
Mdl.prototype.fillDynamicPart = function() {
  this.timer.innerHTML = this.getModelGame().time_left;
};

/**
 * Add a view of a duck
 */
Mdl.prototype.addDuckView = function(modelDuck) {
  var duckView = hprDuckView.init({}, modelDuck, this);
  this.duck_views.push(duckView);
  // draw a duck in a field
  this.wrap.appendChild(duckView.block);

};

/**
 * Generate duck views from models
 */
Mdl.prototype.addDuckViews = function() {
  this.getModelGame().ducks.forEach(this.addDuckView.bind(this));
};

/**
 * Create an instance
 */
exports.init = function() {
  var obj = Object.create(Mdl.prototype);
  Mdl.apply(obj, arguments);
  return obj;
};

module.exports = exports;
