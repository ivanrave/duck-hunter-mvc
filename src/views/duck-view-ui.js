/**
 * Duck view UI
 * @module
 */

var jqr = require('../helpers/jqr');

/**
 * Duck
 * @constructor
 */
var Mdl = function(data, modelDuck, gameView) {
  this.getDuckModel = function() {
    return modelDuck;
  };

  this.getGameView = function() {
    return gameView;
  };

  this.block = document.createElement('div');
  this.duckElem = document.createElement('div');
  this.block.appendChild(this.duckElem);

  this.fillConstPart();
};

/**
 * Fill constant part of a view
 * @private
 */
Mdl.prototype.fillConstPart = function() {
  jqr(this.block).css({
    position: 'absolute'
  });

  var aspectRatio = this.getGameView().aspect_ratio;

  // unchangeable properties from mdlDuck
  jqr(this.duckElem).css({
    background: this.getDuckModel().color,
    width: this.getDuckModel().width * aspectRatio,
    height: this.getDuckModel().height * aspectRatio
  });


  this.getDuckModel().obsSubject.addObserver(this.fillDynamicPart.bind(this));
};

/** 
 * Handle model: fill dynamic props
 * @private
 */
Mdl.prototype.fillDynamicPart = function() {
  if (this.getDuckModel().is_alive === true) {
    var aspectRatio = this.getGameView().aspect_ratio;

    // changeable properties of model
    jqr(this.block).animate({
      top: this.getDuckModel().pos_x * aspectRatio,
      left: this.getDuckModel().pos_y * aspectRatio
    }, 400);
  } else {
    // todo: remove a duck or stop observing of a model
    jqr(this.block).hide();
  }
};

/**
 * Create an instance
 */
exports.init = function() {
  var obj = Object.create(Mdl.prototype);
  Mdl.apply(obj, arguments);
  return obj;
};

module.exports = exports;
