/**
 * Controller view: database
 * @module
 */

/**
 *
 * @constructor
 */
var Mdl = function(data, ctr) {
  Object.keys(data).forEach(this.buildProp.bind(this, data));

  this.ctr = ctr;
};

/**
 * Build a prop for a model
 */
Mdl.prototype.buildProp = function(data, propKey) {
  this[propKey] = data[propKey];
};

Mdl.prototype.loadGame = function() {
  // position of ducks generated on the server side
  //   or in this controller
  var demoDataFromDb = {
    id: 1,
    time_left: 100,
    field_width: 100, // mm
    field_height: 100, // mm
    ducks: [{
      id: 200,
      pos_x: 40, // mm
      pos_y: 59, // mm
      color: '#555',
      width: 12,
      height: 12, // mm
      is_alive: true,
      id_of_game: 1
    }, {
      id: 201,
      pos_x: 50,
      pos_y: 25,
      color: '#999',
      width: 15, //mm
      height: 15, // mm
      is_alive: true,
      id_of_game: 1
    }],
    hunters: []
  };

  this.ctr.initModel(demoDataFromDb);
  for (var i = 0; i < demoDataFromDb.ducks.length; i++) {
    this.ctr.model.addDuck(demoDataFromDb.ducks[i]);
  }
};

/**
 * Create an instance
 */
exports.init = function() {
  var obj = Object.create(Mdl.prototype);
  Mdl.apply(obj, arguments);
  return obj;
};

module.exports = exports;
