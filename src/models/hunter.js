/**
 * Hunter model
 * @module
 */

/**
 * Hunter
 * @constructor
 */
var Mdl = function(data, game) {
  this.getGame = function() {
    return game;
  };
  this.id = data.id;
  this.name = data.name;
  this.score = data.score;
  this.cursor_color = data.cursor_color;
  this.id_of_game = data.id_of_game;
};

/**
 * Create an instance
 */
exports.init = function() {
  var obj = Object.create(Mdl.prototype);
  Mdl.apply(obj, arguments);
  return obj;
};

module.exports = exports;
