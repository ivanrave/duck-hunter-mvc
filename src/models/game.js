/**
 * Model example
 * @module
 */

var hprHunter = require('./hunter.js');
var hprDuck = require('./duck.js');
var obs = require('../helpers/obs');

/**
 * Input data checked, using metadata for this model
 */
var checkData = function(data) {
  var errors = [];
  if (!data.id) {
    errors.push(new Error('required: id'));
  }
  if (data.time_left < 0) {
    errors.push(new Error('required: time_left: positive'));
  }
  return errors;
};

/**
 *
 * @constructor
 */
var Mdl = function(data) {
  this.applyProps(data);

  /** 
   * Add ducks
   *    It is not a part of a model: add it separatelly
   *    through method: addDuck, addHunter
   *    if an error will be catched: do not add only one item, but a model stay be created
   *    Not properties of a model - like calculated properties
   */
  this.ducks = [];
  this.hunters = [];

  this.obsSubject = obs.makeObservableSubject();
};

/**
 * Make an observable model
 */
Mdl.prototype.applyProps = function(data) {
  /**
   * Id of a game
   * @type {Number}
   */
  this.id = data.id;

  /**
   * Time for hunting, in seconds
   * @type {Number}
   */
  this.time_left = data.time_left;

  /**
   * Width of a game
   */
  this.field_width = data.field_width;

  /**
   * Height of a game, in mm
   * @type {Number}
   */
  this.field_height = data.field_height;
};

/**
 * External method to update all model at one time
 */
Mdl.prototype.updateModel = function(data) {
  var errors = checkData(data);
  if (errors.length > 0) {
    return new Error('errors: ' + errors.join(';'));
  }
  this.applyProps(data);
  this.obsSubject.notifyObservers();
  return this;
};

/**
 * Init a duck
 */
Mdl.prototype.initDuck = function(data) {
  return hprDuck.init(data, this);
};

Mdl.prototype.addDuck = function(data) {
  var duckNew = this.initDuck(data);
  // catch error
  this.ducks.push(duckNew);
};

Mdl.prototype.initHunter = function(data) {
  return hprHunter.init(data, this);
};

/**
 * Create an instance
 *    Check data before creation and check every time during update
 */
exports.init = function(data) {
  var errors = checkData(data);
  if (errors.length > 0) {
    throw new Error('errors: ' + errors.join(';'));
  }

  var obj = Object.create(Mdl.prototype);
  Mdl.apply(obj, arguments);
  //  return obj;

  // every time, when model is changed - notify views
  obj.obsSubject.notifyObservers();
  return obj;
};

module.exports = exports;
