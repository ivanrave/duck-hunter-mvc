/**
 * Duck
 * @module
 */

var obs = require('../helpers/obs');

var checkData = function(data, game) {
  var errors = [];
  if (data.pos_x < 0 || data.pos_x > game.field_width) {
    errors.push(new Error('required: pos_x in perimeter: 0-' + game.field_width));
  }

  if (data.pos_y < 0 || data.pos_y > game.field_height) {
    errors.push(new Error('required: pos_y in perimeter: 0-' + game.field_height));
  }

  return errors;
};

/**
 * Duck
 * @constructor
 */
var Mdl = function(data, game) {
  this.getGame = function() {
    return game;
  };

  this.applyProps(data);

  this.obsSubject = obs.makeObservableSubject();
};

/**
 * Apply main props for a model
 *     do not use a game parameter
 */
Mdl.prototype.applyProps = function(data) {
  /**
   * Coords of a duck, mm
   */
  this.pos_x = data.pos_x;
  this.pos_y = data.pos_y;

  /**
   * Whether the duck alive, show or hide on the screen
   * @type {boolean}
   */
  this.is_alive = data.is_alive;

  /**
   * Id of a game, from a storage
   * @type {Number}
   */
  this.id_of_game = data.id_of_game;

  /**
   * Color of a duck, #rgb format
   *    unchangeable after initialization
   * @type {String}
   */
  this.color = data.color;

  /**
   * Width of a duck, in mm
   */
  this.width = data.width;

  /**
   * Height of a duck, in mm
   */
  this.height = data.height;
};

/**
 * Update a whole model
 *    instead separated props
 */
Mdl.prototype.updateModel = function(data) {
  var errors = checkData(data, this.getGame());
  if (errors.length > 0) {
    throw new Error('errors: ' + errors.join(';'));
  }

  this.applyProps(data);
  this.obsSubject.notifyObservers();
};

/**
 * Whether coord in sight
 * @return {Boolean}
 */
var isInBorder = function(sight, min, max) {
  if ((sight > min) && (sight < max)) {
    return true;
  }
  return false;
};

/**
 * Whether a duck in sight
 */
Mdl.prototype.isDuckInSight = function(sightX, sightY) {
  //  console.log('duck-posx', this.pos_y, sightY);
  if (isInBorder(sightX, this.pos_x, this.pos_x + this.width)) {
    //console.log('x in border');
    if (isInBorder(sightY, this.pos_y, this.pos_y + this.height)) {
      //      console.log('in bordder');
      return true;
    }
  }

  return false;
};

/**
 * Create an instance
 */
exports.init = function(data, game) {
  // some of data can depends of a parent
  var errors = checkData(data, game);
  if (errors.length > 0) {
    throw new Error('errors: ' + errors.join(';'));
  }

  var obj = Object.create(Mdl.prototype);
  Mdl.apply(obj, [data, game]);
  obj.obsSubject.notifyObservers();
  return obj;
};

module.exports = exports;
