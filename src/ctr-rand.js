/**
 * Controller view: random flight for all ducks
 * @module
 */

var jqr = require('./helpers/jqr');
var rnd = require('./helpers/rnd');

/**
 *
 * @constructor
 */
var Mdl = function(data, ctr) {
  this.ctr = ctr;
};

/**
 * Generate duck position
 */
Mdl.prototype.gnrtDuckPosition = function(duckItem) {
  var duckData = jqr.extend({}, duckItem);
  duckData.pos_x = rnd.between(0, this.ctr.model.field_width);
  duckData.pos_y = rnd.between(0, this.ctr.model.field_height);
  duckItem.updateModel(duckData);
};

/**
 * Generate random positions for all ducks
 */
Mdl.prototype.gnrtDucksPosition = function() {
  this.ctr.model.ducks.forEach(this.gnrtDuckPosition.bind(this));
};

/**
 * Start a process of flight
 * @param {Number} updateInterval - Update interaval, in seconds
 */
Mdl.prototype.startFlight = function(updateInterval) {
  window.setInterval(this.gnrtDucksPosition.bind(this), updateInterval * 1000);
};

Mdl.prototype.gnrtGameTimeLeft = function(tint) {
  var modelData = jqr.extend({}, this.ctr.model);
  if (modelData.time_left > 0) {
    modelData.time_left = modelData.time_left - 1;

    this.ctr.model.updateModel(modelData);
  } else {
    window.clearInterval(tint);
  }
};

/**
 * Start a timer
 */
Mdl.prototype.startTimer = function() {
  var tint = window.setInterval(this.gnrtGameTimeLeft.bind(this, tint), 1000);
};

/**
 * Create an instance
 */
exports.init = function() {
  var obj = Object.create(Mdl.prototype);
  Mdl.apply(obj, arguments);
  return obj;
};

module.exports = exports;
